<?php

namespace Ayush\StoreBundle\Controller;

use Ayush\StoreBundle\Document\Manufacturer;
use Ayush\StoreBundle\Document\Marketer;
use Ayush\StoreBundle\Document\Product;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

class DefaultController extends Controller
{
    /**
     * @Route("/addProduct")
     */
    public function productAction()
    {
        $dm = $this->get('doctrine_mongodb')->getManager();
        $product = new Product();
        $product->setName('A Foo Bar');
        $product->setPrice('19.99');

        $manufacturer = $dm->getRepository('AyushStoreBundle:Manufacturer')
            ->findOneBy(['name' => 'ayush']);

//        var_dump($manufacturer);
        $product->setManufacturer($manufacturer);
        $dm->persist($product);
        $dm->flush();
        var_dump($product->getId());
        return $this->render('AyushStoreBundle:Default:index.html.twig');
    }

    /**
     * @Route("/addManu")
     */
    public function manufacturerAction()
    {

        $manufacturer = new Manufacturer();
        $manufacturer->setName('ayush');

        $dm = $this->get('doctrine_mongodb')->getManager();
        $dm->persist($manufacturer);
        $dm->flush();
        var_dump($manufacturer->getId());
        return $this->render('AyushStoreBundle:Default:index.html.twig');
    }

    /**
     * @Route("/addMarketer")
     */
    public function marketerAction()
    {

        $manufacturer = new Marketer();
        $manufacturer->setName('Sibananda');
        $manufacturer->setAddress('Bhubaneswar');

        $dm = $this->get('doctrine_mongodb')->getManager();
        $dm->persist($manufacturer);
        $dm->flush();
        var_dump($manufacturer->getId());
        return $this->render('AyushStoreBundle:Default:index.html.twig');
    }

    /**
     * @Route("/showall")
     */
    public function showAction()
    {
        $id = '5936c4c58f805946c82203b8';
        $dm = $this->get('doctrine_mongodb')->getManager();

        $product = $dm->getRepository('AyushStoreBundle:Product')->findAll();
//        var_dump($all_products);
        $all_manufacturer = $dm->getRepository('AyushStoreBundle:Manufacturer')->findAll();
        $all_marketers = $dm->getRepository('AyushStoreBundle:Marketer')->findAll();

        $manufacturer = $dm->getRepository('AyushStoreBundle:Manufacturer')->find($id);
//        var_dump($manufacturer->getName());

//        var_dump($manufacturer->getId());
        $product = $dm->getRepository('AyushStoreBundle:Product')->findBy(['manufacturer.id' => $manufacturer->getId()]);
        $all_product = $dm->getRepository('AyushStoreBundle:Product')->findAll();


//        $product = $this->get('doctrine_mongodb')->getRepository('AyushStoreBundle:Product')->findAll();
//        var_dump($product);
//        $manufacturer = $dm->getRepository('AyushStoreBundle:Manufacturer')->find('5929502d8f80591087775e81')
//        $product = $this->get('doctrine_mongodb')->getRepository('AyushStoreBundle:Product')->findOneBy([]);
//        $manufacturer = $product->getManufacturer();
//        var_dump($manufacturer);

//        if(!$product){
//            throw $this->createNotFoundException('No product found for id '.$id);
//        }
//        $product->setName('ayush');
//        $dm = $this->get('doctrine_mongodb')->getManager();
//        $dm->persist($product);
//        $dm->flush();
//        var_dump($product);
        return $this->render('AyushStoreBundle:Default:showall.html.twig',[
            'products' => $product,
            'manufacturer' => $manufacturer,
            'all_manu' => $all_manufacturer,
            'all_market' => $all_marketers,
            'all_product' => $all_product,
        ]);

    }
}
