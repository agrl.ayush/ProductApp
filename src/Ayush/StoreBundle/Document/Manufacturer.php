<?php
/**
 * Created by PhpStorm.
 * User: arya
 * Date: 27/5/17
 * Time: 12:11 PM
 */

namespace Ayush\StoreBundle\Document;


use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ODM\MongoDB\Mapping\Annotations as MongoDB;


/**
 *
 * @MongoDB\InheritanceType("SINGLE_COLLECTION")
 * @MongoDB\DiscriminatorField("type")
 * @MongoDB\DiscriminatorMap({"manu"="Manufacturer", "marketer"="Marketer"})
 *
 * @MongoDB\DefaultDiscriminatorValue("manu")
 *
 * @MongoDB\Document
 */
class Manufacturer
{
    /**
     * @Doctrine\ODM\MongoDB\Mapping\Annotations\Id(strategy="AUTO", type="string")
     */
    protected $id;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @MongoDB\Field(type="string")
     */
    protected $name;

    /**
     * @MongoDB\ReferenceMany(targetDocument="Ayush\StoreBundle\Document\Product", mappedBy="manufacturer")
     */
    protected $products;

    /**
     * Set name
     *
     * @param string $name
     * @return $this
     */
    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }

    /**
     * Get name
     *
     * @return string $name
     */
    public function getName()
    {
        return $this->name;
    }

    public function __construct()
    {
        $this->products = new \Doctrine\Common\Collections\ArrayCollection();
    }
    
    /**
     * Add product
     *
     * @param Ayush\StoreBundle\Document\Product $product
     */
    public function addProduct(\Ayush\StoreBundle\Document\Product $product)
    {
        $this->products[] = $product;
    }

    /**
     * Remove product
     *
     * @param Ayush\StoreBundle\Document\Product $product
     */
    public function removeProduct(\Ayush\StoreBundle\Document\Product $product)
    {
        $this->products->removeElement($product);
    }

    /**
     * Get products
     *
     * @return \Doctrine\Common\Collections\Collection $products
     */
    public function getProducts()
    {
        return $this->products;
    }
}
