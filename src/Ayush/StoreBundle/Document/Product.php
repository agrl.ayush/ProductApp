<?php
/**
 * Created by PhpStorm.
 * User: arya
 * Date: 27/5/17
 * Time: 12:55 AM
 */
namespace Ayush\StoreBundle\Document;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ODM\MongoDB\Mapping\Annotations as MongoDB;


/**
 *
 * @MongoDB\Document
 */
Class Product
{

    public function __construct()
    {
//        $this->manufacturer = new ArrayCollection();
    }
    /**
     * @Doctrine\ODM\MongoDB\Mapping\Annotations\Id(strategy="AUTO", type="string")
     */
    protected $id;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @MongoDB\Field(type="string")
     */
    protected $name;

    /**
     * @MongoDB\Field(type="float")
     */
    protected $price;

    /**
     * @MongoDB\ReferenceOne(targetDocument="Ayush\StoreBundle\Document\Manufacturer", inversedBy="products")
     *
     */
    protected $manufacturer;

    /**
     * @return
     */
    public function getManufacturer()
    {
        return $this->manufacturer;
    }

    /**
     * @param Manufacturer $manufacturer
     */
    public function setManufacturer(Manufacturer $manufacturer)
    {
        $this->manufacturer = $manufacturer;
    }


    /**
     * Set name
     *
     * @param string $name
     * @return $this
     */
    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }

    /**
     * Get name
     *
     * @return string $name
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set price
     *
     * @param float $price
     * @return $this
     */
    public function setPrice($price)
    {
        $this->price = $price;
        return $this;
    }

    /**
     * Get price
     *
     * @return float $price
     */
    public function getPrice()
    {
        return $this->price;
    }
}
